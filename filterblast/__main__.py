"""
Usage:
    filterblast <e_value> [<column> = -1]
    
Description:
    Filters a BLAST file on e-value. Reads from STD.IN to STD.OUT.
"""

import sys


if len( sys.argv ) < 2:
    print(sys.modules[__name__].__doc__, file = sys.stderr)
    exit( 1 )

e_value_cutoff = float( sys.argv[ 1 ] )
e_value_column = int( sys.argv[ 2 ] ) if len( sys.argv ) >= 3 else -1

rejected = 0
accepted = 0

print( "E-VALUE: {}".format( e_value_cutoff ), file = sys.stderr )
print( "COLUMN:  {}".format( e_value_column ), file = sys.stderr )

for line in sys.stdin:
    columns = line.split( "\t" )
    e_value = float( columns[ e_value_column ].strip() )
    
    if e_value < e_value_cutoff:
        sys.stdout.write( line )
        accepted += 1
    else:
        rejected += 1

print( "{} accepted, {} rejected, {} total".format( accepted, rejected, accepted + rejected ), file = sys.stderr )
exit( 0 )
