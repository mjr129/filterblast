# Filter BLAST

Filters a BLAST file on e-value

## Usage

`filterblast <e-value> <column>`

* `<e_value>` is the e-value cutoff. It is mandatory.
* `<column>` defaults to `-1` if not specified (the last column) if not specified.
* `STDIN` existing BLAST
* `STDOUT` new BLAST

## Example

`filterblast 1e-6 < plasmids.blast > plasmids-filtered.blast`

Meta
----

```
author= Martin Rusilowicz
license= GPLv3
date= 2017
language= Python3
type=application,application-cli
hosts=bitbucket
```
